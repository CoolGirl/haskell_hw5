{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, UndecidableInstances #-}
module Maybe where
import Monad
import GHC.Show

data Maybe a = Nothing | Just a deriving Show

instance Monad Maybe where
    return = Just

    Nothing >>= _ = Nothing
    Just a >>= f = f a