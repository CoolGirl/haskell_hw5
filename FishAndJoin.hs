{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, UndecidableInstances #-}
module FishAndJoin where
import Maybe
import Prelude (id)

class Monad m where
    return :: a -> m a
    (>>=)  :: m a -> (a -> m b) -> m b

class MonadFish m where
    returnFish :: a -> m a
    (>=>)      :: (a -> m b) -> (b -> m c) -> (a -> m c)

class MonadJoin m where
    returnJoin :: a -> m a
    join       :: m (m a) -> m a

instance Monad m => MonadFish m where
    returnFish = return
    f >=> g    = \x -> f x >>= g

instance Monad m => MonadJoin m where
    returnJoin = return
    join m = m >>= id
