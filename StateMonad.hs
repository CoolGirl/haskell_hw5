
module StateMonad where
import Control.Monad
import Control.Applicative (Applicative(..))
import Control.Monad       (liftM, ap)

instance Functor (State s) where
    fmap = liftM

instance Applicative (State s) where
    pure a = State (\s -> (a, s))
    (<*>) = ap

instance Monad (State s) where
    return = pure
    m >>= g = State (\s ->  let (a, s1) = getFun m s in
                            let state' = g a in
                            getFun state' s1)

newtype State s a = State {getFun :: s -> (a, s)}