module GameStrategy where

manHeaps :: (Int, Int) -> [(Int, Int)]
manHeaps (a, b) = filter isCorrectHeaps
    [ (a - 1, b), (a * 2, b `div` 2)
    , (a , b - 1), (a `div` 2, b * 2)
    ]
   where
     isCorrectHeaps (x, y) = x >= 0 && y >= 0

zeroInMin :: (Int, Int) -> Int
zeroInMin (a,b) | a < 0 || b < 0 = undefined
zeroInMin pair = zeroInMin' [pair] 0

zeroInMin' :: [(Int, Int)] -> Int -> Int
zeroInMin' pairs counter = if isEndOfGame pairs then
                             counter else
                             zeroInMin' (pairs >>= manHeaps) counter + 1

isEndOfGame :: [(Int, Int)] -> Bool
isEndOfGame pairs = any (\(a, b) -> a == 0 && b == 0) $ pairs