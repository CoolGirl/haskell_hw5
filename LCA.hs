module LCA where

import Data.List
import Control.Monad
import Data.Maybe

data InNode a = Node { label :: a, parent :: Maybe (InNode a)}  deriving Show

leastCommonAncestor :: Eq a => InNode a -> InNode a -> Maybe (InNode a)
leastCommonAncestor a b | label a == label b = Just a
leastCommonAncestor a b = join $ find (\x ->
                                        any (\y ->
                                             label (fromJust y) == label (fromJust x))
                                        (pathToRoot (Just a)))
                                 (pathToRoot (Just b))

pathToRoot :: Maybe (InNode a) -> [Maybe (InNode a)]
pathToRoot Nothing = []
pathToRoot a =  a : pathToRoot (a >>= parent)
