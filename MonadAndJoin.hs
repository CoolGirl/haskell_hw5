{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, UndecidableInstances #-}
module MonadAndJoin where
import Prelude (id)

class MonadFish m where
    returnFish :: a -> m a
    (>=>)      :: (a -> m b) -> (b -> m c) -> (a -> m c)

class MonadJoin m where
    returnJoin :: a -> m a
    join       :: m (m a) -> m a

class Monad m where
    return :: a -> m a
    (>>=)  :: m a -> (a -> m b) -> m b

instance MonadFish m => Monad m where
    return = returnFish
    m >>= g = ((\_ -> m) >=> g) ()

instance MonadFish m => MonadJoin m where
    returnJoin = returnFish
    join = id >=> id